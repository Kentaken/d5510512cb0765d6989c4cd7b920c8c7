from django.contrib import admin

from . import models


class KeyAdmin(admin.ModelAdmin):

    def has_delete_permission(self, *args, **kwargs):
        return False


admin.site.register(models.Key, KeyAdmin)
