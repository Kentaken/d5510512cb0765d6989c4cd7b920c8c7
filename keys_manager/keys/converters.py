from . import local_settings
from . import consts


class StringConverter:

    regex = '[^/]+'

    def to_python(self, value):
        return value

    def to_url(self, value):
        return value


class KeyConverter(StringConverter):

    regex = f'[{consts.ALPHABET}]{{{local_settings.KEY_LENGTH}}}'
