from string import ascii_letters, digits

from . import local_settings

#: alphabet for key generation
ALPHABET = digits + ascii_letters
#: inversed alphabet for fast index searching of char in alphabet
INVERSE_ALPHABET = {c: i for i, c in enumerate(ALPHABET)}
#: length of alphabet
ALPHABET_LENGTH = len(ALPHABET)
#: calculated total key amount by alphabet length and key length
KEYS_TOTAL = ALPHABET_LENGTH ** local_settings.KEY_LENGTH
