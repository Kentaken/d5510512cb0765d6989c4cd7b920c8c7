from django.db import models


class Key(models.Model):

    #: indicates that the key is used
    used = models.BooleanField(verbose_name='Used', default=False, blank=False, null=False)

    class Meta:
        verbose_name = 'key'
        verbose_name_plural = 'keys'
