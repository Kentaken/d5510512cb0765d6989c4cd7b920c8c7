from django.views import View
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseNotFound

from . import models
from . import consts
from . import local_settings


def key_to_number(key: str) -> int:
    """Returns number associated with specified key."""
    result = 0
    for i, c in enumerate(reversed(key)):
        if c not in consts.INVERSE_ALPHABET:
            raise ValueError("Invalid key")
        result += consts.INVERSE_ALPHABET[c] * consts.ALPHABET_LENGTH ** i
    return result


def number_to_key(number: int) -> str:
    """Returns key associated with specified number."""
    result = []
    while number != 0:
        r = number % consts.ALPHABET_LENGTH
        number = number // consts.ALPHABET_LENGTH
        result.append(consts.ALPHABET[r])
    return ''.join(reversed(result)).rjust(local_settings.KEY_LENGTH, consts.ALPHABET[0])


class KeyIssue(View):

    """Provides unique keys to clients."""

    def get(self, request):
        key = models.Key()
        key.save()

        if key.id > consts.KEYS_TOTAL:
            return HttpResponseBadRequest("All keys have already been issued")

        return HttpResponse(number_to_key(key.id - 1))


class KeyUse(View):

    """Marks keys as used."""

    def get(self, request, key):
        try:
            key = models.Key.objects.get(pk=key_to_number(key) + 1)
        except models.Key.DoesNotExist:
            return HttpResponseNotFound("This key is not issued")

        if key.used:
            return HttpResponseBadRequest("The key already marked as used")

        key.used = True
        key.save()

        return HttpResponse("Ok")


class KeyInfo(View):

    """Returns state of received keys."""

    def get(self, request, key):
        try:
            key = models.Key.objects.get(pk=key_to_number(key) + 1)
        except models.Key.DoesNotExist:
            return HttpResponse('Not issued')

        if key.used:
            return HttpResponse('Used')

        return HttpResponse('Issued')


class KeysInfo(View):

    """Returns number not issued keys."""

    def get(self, request):
        try:
            latest_id = models.Key.objects.values_list('id', flat=True).latest('id')
        except models.Key.DoesNotExist:
            return HttpResponse(consts.KEYS_TOTAL)

        return HttpResponse(consts.KEYS_TOTAL - latest_id)
