from django.urls import path, register_converter

from . import views, converters


register_converter(converters.KeyConverter, 'key')

urlpatterns = [
    path('issue/', views.KeyIssue.as_view()),
    path('use/<key:key>/', views.KeyUse.as_view()),
    path('info/<key:key>/', views.KeyInfo.as_view()),
    path('info/', views.KeysInfo.as_view())
]
