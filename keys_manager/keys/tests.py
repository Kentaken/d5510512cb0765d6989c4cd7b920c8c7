from django.test import TestCase, Client

from . import consts


class KeysTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_key_issue(self):
        resp = self.client.get('/keys/issue/')
        self.assertEqual(b'0000', resp.content)
        self.assertEqual(200, resp.status_code)

        resp = self.client.get('/keys/issue/')
        self.assertEqual(b'0001', resp.content)
        self.assertEqual(200, resp.status_code)

    def test_key_info(self):
        resp = self.client.get('/keys/info/0000/')
        self.assertEqual(b'Not issued', resp.content)
        self.assertEqual(200, resp.status_code)

        resp = self.client.get('/keys/issue/')
        self.assertEqual(b'0000', resp.content)
        self.assertEqual(200, resp.status_code)

        resp = self.client.get('/keys/info/0000/')
        self.assertEqual(b'Issued', resp.content)
        self.assertEqual(200, resp.status_code)

        resp = self.client.get('/keys/use/0000/')
        self.assertEqual(b'Ok', resp.content)
        self.assertEqual(200, resp.status_code)

        resp = self.client.get('/keys/info/0000/')
        self.assertEqual(b'Used', resp.content)
        self.assertEqual(200, resp.status_code)

    def test_keys_info(self):
        resp = self.client.get('/keys/info/')
        self.assertEqual(consts.KEYS_TOTAL, int(resp.content))
        self.assertEqual(200, resp.status_code)

        resp = self.client.get('/keys/issue/')
        self.assertEqual(b'0000', resp.content)
        self.assertEqual(200, resp.status_code)

        resp = self.client.get('/keys/info/')
        self.assertEqual(consts.KEYS_TOTAL - 1, int(resp.content))
        self.assertEqual(200, resp.status_code)

    def test_use_not_issued_key(self):
        resp = self.client.get('/keys/use/0000/')
        self.assertEqual(b"This key is not issued", resp.content)
        self.assertEqual(404, resp.status_code)

    def test_successfully_use_key(self):
        resp = self.client.get('/keys/issue/')
        self.assertEqual(b'0000', resp.content)
        self.assertEqual(200, resp.status_code)

        resp = self.client.get('/keys/use/0000/')
        self.assertEqual(b"Ok", resp.content)
        self.assertEqual(200, resp.status_code)

    def test_attempt_use_already_used_key(self):
        resp = self.client.get('/keys/issue/')
        self.assertEqual(b'0000', resp.content)
        self.assertEqual(200, resp.status_code)

        resp = self.client.get('/keys/use/0000/')
        self.assertEqual(b"Ok", resp.content)
        self.assertEqual(200, resp.status_code)

        resp = self.client.get('/keys/use/0000/')
        self.assertEqual(b"The key already marked as used", resp.content)
        self.assertEqual(400, resp.status_code)
